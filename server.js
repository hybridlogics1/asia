/* eslint-disable */


if (process.env.APPLICATION_NAME === 'API') {
  require('./server/');
}
else if(process.env.APPLICATION_NAME === 'GRAPHQL') {
  require('./graphql/');
}
else if(process.env.APPLICATION_NAME === 'ADMIN') {
  require('./admin/');
}
else if(process.env.APPLICATION_NAME === 'CLIENT') {
  require('./client/');
}
else {
  // local all apllications in one process
  require('./server/');

}