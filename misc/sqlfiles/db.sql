--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.0

-- Started on 2017-12-17 14:49:07 +07

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4 (class 2615 OID 29908)
-- Name: main; Type: SCHEMA; Schema: -; Owner: root
--

CREATE SCHEMA main;


ALTER SCHEMA main OWNER TO root;

--
-- TOC entry 1 (class 3079 OID 29910)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 3018 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = main, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 197 (class 1259 OID 29915)
-- Name: accesstoken; Type: TABLE; Schema: main; Owner: root
--

CREATE TABLE accesstoken (
    id text NOT NULL,
    ttl integer DEFAULT 1209600,
    created timestamp with time zone,
    userid bigint
);


ALTER TABLE accesstoken OWNER TO root;

--
-- TOC entry 198 (class 1259 OID 29922)
-- Name: acl; Type: TABLE; Schema: main; Owner: root
--

CREATE TABLE acl (
    model text,
    property text,
    accesstype text,
    permission text,
    principaltype text,
    principalid text,
    id integer NOT NULL
);


ALTER TABLE acl OWNER TO root;

--
-- TOC entry 199 (class 1259 OID 29928)
-- Name: acl_id_seq; Type: SEQUENCE; Schema: main; Owner: root
--

CREATE SEQUENCE acl_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE acl_id_seq OWNER TO root;

--
-- TOC entry 3019 (class 0 OID 0)
-- Dependencies: 199
-- Name: acl_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: root
--

ALTER SEQUENCE acl_id_seq OWNED BY acl.id;


--
-- TOC entry 290 (class 1259 OID 30325)
-- Name: role; Type: TABLE; Schema: main; Owner: root
--

CREATE TABLE role (
    id integer NOT NULL,
    name text NOT NULL,
    description text,
    created timestamp with time zone,
    modified timestamp with time zone
);


ALTER TABLE role OWNER TO root;

--
-- TOC entry 291 (class 1259 OID 30331)
-- Name: role_id_seq; Type: SEQUENCE; Schema: main; Owner: root
--

CREATE SEQUENCE role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE role_id_seq OWNER TO root;

--
-- TOC entry 3065 (class 0 OID 0)
-- Dependencies: 291
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: root
--

ALTER SEQUENCE role_id_seq OWNED BY role.id;


--
-- TOC entry 292 (class 1259 OID 30333)
-- Name: rolemapping; Type: TABLE; Schema: main; Owner: root
--

CREATE TABLE rolemapping (
    id integer NOT NULL,
    principaltype text,
    principalid text,
    roleid integer
);


ALTER TABLE rolemapping OWNER TO root;

--
-- TOC entry 293 (class 1259 OID 30339)
-- Name: rolemapping_id_seq; Type: SEQUENCE; Schema: main; Owner: root
--

CREATE SEQUENCE rolemapping_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE rolemapping_id_seq OWNER TO root;

--
-- TOC entry 3066 (class 0 OID 0)
-- Dependencies: 293
-- Name: rolemapping_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: root
--

ALTER SEQUENCE rolemapping_id_seq OWNED BY rolemapping.id;




--
-- TOC entry 298 (class 1259 OID 30359)
-- Name: suppliers; Type: TABLE; Schema: main; Owner: root
--

CREATE TABLE suppliers (
    id integer NOT NULL,
    name character varying NOT NULL,
    public_phone_number character varying NOT NULL,
    address character varying NOT NULL,
    reservation_email character varying NOT NULL,
    secondary_email character varying,
    business_phone_number character varying NOT NULL,
    fax character varying,
    nationality character varying,
    country_id bigint,
    created_at timestamp without time zone DEFAULT '2017-11-12 20:20:58.502262'::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE suppliers OWNER TO root;

--
-- TOC entry 299 (class 1259 OID 30366)
-- Name: suppliers_accounting; Type: TABLE; Schema: main; Owner: root
--

CREATE TABLE suppliers_accounting (
    id integer NOT NULL,
    supplier_id bigint NOT NULL,
    bank_account character varying NOT NULL,
    holder character varying NOT NULL,
    account_number character varying NOT NULL,
    branch character varying NOT NULL,
    branch_code character varying NOT NULL,
    swift_code character varying NOT NULL,
    created_at timestamp without time zone DEFAULT '2017-11-12 20:20:58.503486'::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE suppliers_accounting OWNER TO root;

--
-- TOC entry 300 (class 1259 OID 30373)
-- Name: suppliers_accounting_id_seq; Type: SEQUENCE; Schema: main; Owner: root
--

CREATE SEQUENCE suppliers_accounting_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE suppliers_accounting_id_seq OWNER TO root;

--
-- TOC entry 3069 (class 0 OID 0)
-- Dependencies: 300
-- Name: suppliers_accounting_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: root
--

ALTER SEQUENCE suppliers_accounting_id_seq OWNED BY suppliers_accounting.id;


--
-- TOC entry 301 (class 1259 OID 30375)
-- Name: suppliers_billing; Type: TABLE; Schema: main; Owner: root
--

CREATE TABLE suppliers_billing (
    id integer NOT NULL,
    supplier_id bigint NOT NULL,
    supplier_payment_type character varying NOT NULL,
    currency_id bigint NOT NULL,
    created_at timestamp without time zone DEFAULT '2017-11-12 20:20:58.501949'::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone
);


ALTER TABLE suppliers_billing OWNER TO root;

--
-- TOC entry 302 (class 1259 OID 30382)
-- Name: suppliers_billing_id_seq; Type: SEQUENCE; Schema: main; Owner: root
--

CREATE SEQUENCE suppliers_billing_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE suppliers_billing_id_seq OWNER TO root;

--
-- TOC entry 3070 (class 0 OID 0)
-- Dependencies: 302
-- Name: suppliers_billing_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: root
--

ALTER SEQUENCE suppliers_billing_id_seq OWNED BY suppliers_billing.id;


--
-- TOC entry 303 (class 1259 OID 30384)
-- Name: suppliers_id_seq; Type: SEQUENCE; Schema: main; Owner: root
--

CREATE SEQUENCE suppliers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE suppliers_id_seq OWNER TO root;

--
-- TOC entry 3071 (class 0 OID 0)
-- Dependencies: 303
-- Name: suppliers_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: root
--

ALTER SEQUENCE suppliers_id_seq OWNED BY suppliers.id;


--
-- TOC entry 304 (class 1259 OID 30386)
-- Name: users; Type: TABLE; Schema: main; Owner: root
--

CREATE TABLE users (
    id integer NOT NULL,
    email character varying NOT NULL,
    password character varying NOT NULL,
    terms_accepted_at timestamp without time zone DEFAULT '2017-11-12 20:20:58.517981'::timestamp without time zone NOT NULL,
    first_name character varying,
    last_name character varying,
    birthday date,
    passport_number character varying,
    phone character varying,
    country_id bigint,
    supplier_id bigint DEFAULT 0,
    created_at timestamp without time zone DEFAULT '2017-11-12 20:20:58.517981'::timestamp without time zone NOT NULL,
    updated_at timestamp without time zone,
    realm text,
    username text,
    emailverified boolean,
    verificationtoken text
);


ALTER TABLE users OWNER TO root;

--
-- TOC entry 305 (class 1259 OID 30395)
-- Name: users_id_seq; Type: SEQUENCE; Schema: main; Owner: root
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO root;

--
-- TOC entry 3072 (class 0 OID 0)
-- Dependencies: 305
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: main; Owner: root
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- TOC entry 2661 (class 2604 OID 30397)
-- Name: acl id; Type: DEFAULT; Schema: main; Owner: root
--

ALTER TABLE ONLY acl ALTER COLUMN id SET DEFAULT nextval('acl_id_seq'::regclass);



--
-- TOC entry 2763 (class 2604 OID 30443)
-- Name: role id; Type: DEFAULT; Schema: main; Owner: root
--

ALTER TABLE ONLY role ALTER COLUMN id SET DEFAULT nextval('role_id_seq'::regclass);


--
-- TOC entry 2764 (class 2604 OID 30444)
-- Name: rolemapping id; Type: DEFAULT; Schema: main; Owner: root
--

ALTER TABLE ONLY rolemapping ALTER COLUMN id SET DEFAULT nextval('rolemapping_id_seq'::regclass);


--
-- TOC entry 2770 (class 2604 OID 30447)
-- Name: suppliers id; Type: DEFAULT; Schema: main; Owner: root
--

ALTER TABLE ONLY suppliers ALTER COLUMN id SET DEFAULT nextval('suppliers_id_seq'::regclass);


--
-- TOC entry 2772 (class 2604 OID 30448)
-- Name: suppliers_accounting id; Type: DEFAULT; Schema: main; Owner: root
--

ALTER TABLE ONLY suppliers_accounting ALTER COLUMN id SET DEFAULT nextval('suppliers_accounting_id_seq'::regclass);


--
-- TOC entry 2774 (class 2604 OID 30449)
-- Name: suppliers_billing id; Type: DEFAULT; Schema: main; Owner: root
--

ALTER TABLE ONLY suppliers_billing ALTER COLUMN id SET DEFAULT nextval('suppliers_billing_id_seq'::regclass);


--
-- TOC entry 2778 (class 2604 OID 30450)
-- Name: users id; Type: DEFAULT; Schema: main; Owner: root
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- TOC entry 2780 (class 2606 OID 30997)
-- Name: accesstoken accesstoken_pkey; Type: CONSTRAINT; Schema: main; Owner: root
--

ALTER TABLE ONLY accesstoken
    ADD CONSTRAINT accesstoken_pkey PRIMARY KEY (id);


--
-- TOC entry 2782 (class 2606 OID 30999)
-- Name: acl acl_pkey; Type: CONSTRAINT; Schema: main; Owner: root
--

ALTER TABLE ONLY acl
    ADD CONSTRAINT acl_pkey PRIMARY KEY (id);



--
-- TOC entry 2874 (class 2606 OID 31091)
-- Name: role role_pkey; Type: CONSTRAINT; Schema: main; Owner: root
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- TOC entry 2876 (class 2606 OID 31093)
-- Name: rolemapping rolemapping_pkey; Type: CONSTRAINT; Schema: main; Owner: root
--

ALTER TABLE ONLY rolemapping
    ADD CONSTRAINT rolemapping_pkey PRIMARY KEY (id);


--
-- TOC entry 2884 (class 2606 OID 31099)
-- Name: suppliers_accounting suppliers_accounting_pkey; Type: CONSTRAINT; Schema: main; Owner: root
--

ALTER TABLE ONLY suppliers_accounting
    ADD CONSTRAINT suppliers_accounting_pkey PRIMARY KEY (id);


--
-- TOC entry 2886 (class 2606 OID 31101)
-- Name: suppliers_billing suppliers_billing_pkey; Type: CONSTRAINT; Schema: main; Owner: root
--

ALTER TABLE ONLY suppliers_billing
    ADD CONSTRAINT suppliers_billing_pkey PRIMARY KEY (id);


--
-- TOC entry 2882 (class 2606 OID 31103)
-- Name: suppliers suppliers_pkey; Type: CONSTRAINT; Schema: main; Owner: root
--

ALTER TABLE ONLY suppliers
    ADD CONSTRAINT suppliers_pkey PRIMARY KEY (id);


--
-- TOC entry 2889 (class 2606 OID 31105)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: main; Owner: root
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2887 (class 1259 OID 31123)
-- Name: users_email_idx; Type: INDEX; Schema: main; Owner: root
--

CREATE UNIQUE INDEX users_email_idx ON users USING btree (email);


--
-- TOC entry 3017 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: root
--

GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2017-12-17 14:49:07 +07

--
-- PostgreSQL database dump complete
--

