const chalk = require('chalk');
const program = require('commander');
console.log("Auto update module checking for input....");
module.exports = function (app) {
    var path = require('path');
    var models = app.models;
    var datasources = app.datasources.theasia;
    function autoUpdateAll() {
        
        Object.keys(models).forEach(function (key) {
            if(!(key == "container" || key == "Container" )){
                datasources.isActual(key, function (err, actual) {
                    if (!actual) {
                        datasources.autoupdate(key, function (err) {
                            if (err) throw err;
                            console.log('Model ' + key + ' updated');
                        });
                    }
                    else {
                        console.log('No update required');
                    }
                });
            }
        });
    }

    program.command('autoupdate').alias('update-db').description('update db with existing models').action(function () {
        console.log(chalk.blue("Auto update called"));
        autoUpdateAll();
    });
    program.parse(process.argv);
};