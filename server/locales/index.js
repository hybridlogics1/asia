const i18n = require('i18n');
const config = require('../config');
const baseUrl = `http://${config.host}:${config.port}`;

const locales = {
  init : function () {
    i18n.configure({
      locales: ['en', 'ko', 'zh', 'th'],
      directory: __dirname + '/'
    });
    i18n.init;
  },
  translatation : function (message) {
    var translate = i18n.__(message);
    return translate;
  }
}
locales.init();
module.exports = locales;