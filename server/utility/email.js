var app = require("../../server/index");
var loopback = require("loopback");
var path = require("path");
var sgMail = require('@sendgrid/mail');
sgMail.setApiKey("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");
var  i18n = require("i18n");

module.exports.send = (lang_id,data, email_type,cb) => {
    if(!lang_id || !email_type || (!data.user && !data.to)){
        cb("Email is not sent",null);
    }
    app.models.Languages.findOne({where:{id:lang_id}},function(err,lang){
        if(err){
            cb("Email is not sent", null);
        }else{
            try {
                if(lang.code){
                    if(email_type=="reset_password"){
                        i18n.configure({
                            locales:['en',lang.code],
                            directory: __dirname + '/locales/reset'
                        });
                        i18n.setLocale(lang.code);
                        var myMessage = {url:data.url,p1:i18n.__('paragraph1'),p2:i18n.__('paragraph2'),p3:i18n.__('paragraph3'),p4:i18n.__('paragraph4'),p5:i18n.__('paragraph5')}; 
                        var renderer = loopback.template(path.resolve(__dirname, '../../common/views/Reset Password/'+lang.code+'.ejs'));
                        html_body = renderer(myMessage);
                        var email = {
                            to: data.to,
                            from: 'noreply@theasia.com',
                            subject: 'Reset Password',
                            text: 'Reset Your Password',
                            html: html_body
                          };
                          sgMail.send(email,function(err,success){
                              if(err){
                                cb("There is an error in sending your email", null);                                  
                              }
                              else{
                                cb(null,"Email sent successfully");
                              }
                          });
                    }
                    if(email_type=="verification"){
                        i18n.configure({
                            locales:['en',lang.code],
                            directory: __dirname + '/locales/verify'
                        });
                        i18n.setLocale(lang.code);
                        var myMessage = {name:data.name,url:data.url,p1:i18n.__('paragraph1'),p2:i18n.__('paragraph2'),p3:i18n.__('paragraph3'),p4:i18n.__('paragraph4'),p5:i18n.__('paragraph5')}; 
                        var renderer = loopback.template(path.resolve(__dirname, '../../common/views/welcome/'+lang.code+'.ejs'));
                        html_body = renderer(myMessage);
                        var email = {
                            to: data.to,
                            from: 'noreply@theasia.com',
                            subject: 'Reset Password',
                            text: 'Reset Your Password',
                            html: html_body
                          };                          
                          sgMail.send(email,function(err,success){
                              if(err){
                                cb("There is an error in sending your email", null);                                  
                              }
                              else{
                                cb(null,"Email sent successfully");
                              }
                          });
                    }
                    if(email_type=="booking"){
                        i18n.configure({
                            locales:['en',lang.code],
                            directory: __dirname + '/locales/booking'
                        });
                        i18n.setLocale(lang.code);
                        var myMessage = {name:data.name,paragraph1:i18n.__('paragraph1'),paragraph2:i18n.__('paragraph2'),paragraph3:i18n.__('paragraph3'),paragraph4:i18n.__('paragraph4'),paragraph5:i18n.__('paragraph5'),paragraph6:i18n.__('paragraph6'),paragraph7:data.paragraph7,paragraph8:data.paragraph8,paragraph9:data.paragraph9,paragraph10:i18n.__('paragraph10'),paragraph11:data.paragraph11,paragraph12:i18n.__('paragraph12'),paragraph13:data.paragraph13,paragraph14:data.paragraph14,paragraph15:i18n.__('paragraph15'),paragraph16:data.paragraph16,paragraph17:i18n.__('paragraph17'),paragraph18:i18n.__('paragraph18')}; 
                        var renderer = loopback.template(path.resolve(__dirname, '../../common/views/booking/'+lang.code+'.ejs'));
                        html_body = renderer(myMessage);
                        var email = {
                            to: data.user,
                            from: 'noreply@theasia.com',
                            subject: 'Booking',
                            text: 'Booking',
                            html: html_body
                          };                          
                          sgMail.send(email,function(err,success){
                              if(err){
                                cb("There is an error in sending your email, Mailer server error Or User"+err, null);                                  
                              }
                              else{
                                i18n.configure({
                                    locales:['en',lang.code],
                                    directory: __dirname + '/locales/booking'
                                });
                                i18n.setLocale('en');
                                var myMessage = {name:data.name,paragraph1:i18n.__('paragraph1'),paragraph2:i18n.__('paragraph2'),paragraph3:i18n.__('paragraph3'),paragraph4:i18n.__('paragraph4'),paragraph5:i18n.__('paragraph5'),paragraph6:i18n.__('paragraph6'),paragraph7:data.paragraph7,paragraph8:data.paragraph8,paragraph9:data.paragraph9,paragraph10:i18n.__('paragraph10'),paragraph11:data.paragraph11,paragraph12:i18n.__('paragraph12'),paragraph13:data.paragraph13,paragraph14:data.paragraph14,paragraph15:i18n.__('paragraph15'),paragraph16:data.paragraph16,paragraph17:i18n.__('paragraph17'),paragraph18:i18n.__('paragraph18')}; 
                                var renderer = loopback.template(path.resolve(__dirname, '../../common/views/booking/en.ejs'));
                                html_body = renderer(myMessage);
                                console.log("Data.supplier ",data.supplier);
                                var email = {
                                    to: data.supplier,
                                    from: 'noreply@theasia.com',
                                    subject: 'Booking',
                                    text: 'Booking',
                                    html: html_body
                                  };                          
                                  sgMail.send(email,function(err,success){
                                      if(err){
                                        cb("There is an error in sending your email, Mailer service error Or supplier", null);                                  
                                      }
                                      else{
                                        cb(null,"Email sent successfully");
                                      }
                                  });
                              }
                          });
                    }
            }else{
                cb("There is an error in sending your email, Language or email not found", null);                                  
            }                
            } catch (error) { 
             cb("There is an error in sending email,something went wrong",null);
            }

        }
    })
}