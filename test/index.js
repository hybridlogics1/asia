// Run test : mocha test/*.js --reporter mochawesome --reporter-options reportDir="./dev"
var chai = require('chai');
var chaiHttp = require('chai-http');
var server = require('../server/index.js');
var expect = require('chai').expect;
var should = chai.should();
chai.use(chaiHttp);

// Server status Test : should return a reposnse json with httpStatus 200 
describe('Server Status', function () {

    it('Ping to server : should give a pong', function (done) {
        chai.request(server)
            .get('/ping')
            .end(function (err, res) {
                if (err) {
                    err.should.have.status(401);
                }
                else {
                    res.should.have.status(200);
                    res.should.be.json;
                    res.body.should.have.property('pong', 1);
                }
                done();
            });
    });
});



