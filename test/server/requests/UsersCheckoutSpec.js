'use strict';

require('../database-cleaner');
var chai = require('chai');
var expect = chai.expect;
var chaiHttp = require('chai-http');
var app = require('../../../server/index');
var baseEndpoint = '/Users';
var config = require('../../../server/config');
var locales = require('../../../server/locales');
var generator = require('generate-password');

chai.use(chaiHttp);
var validGuestUser = {
  email: 'elliotalderson@protonmail.com',
  firstName: 'FirstName',
  lastName: 'LastName',
  passportNumber: 'passsport',
  phone: '+6612345678',
  nationality:'Thai',
  acceptTerms: true
}

describe('Guest', function () {
  describe('POST /Users ( create guest user )', function () {
    describe('Validate guest invalid user', function () {
      it('First name missing request status (422)', function (done) {
        var Users = app.models.Users;
        var guestUser = Object.assign({}, validGuestUser);
        var password = generator.generate({
          length: 10,
          numbers: true
        });
        guestUser.password = password;
        delete guestUser.firstName;
  
        Users.createGuest(guestUser, function (result) {
          expect(result.statusCode).to.eql(422);
          done();
        });
      });

      it('Last name missing request status (422)', function (done) {
        var Users = app.models.Users;
        var guestUser = Object.assign({}, validGuestUser);
        var password = generator.generate({
          length: 10,
          numbers: true
        });
        guestUser.password = password;
        delete guestUser.lastName;
  
        Users.createGuest(guestUser, function (result) {
          expect(result.statusCode).to.eql(422);
          done();
        });
      });

      it('Nationality missing request status (422)', function (done) {
        var Users = app.models.Users;
        var guestUser = Object.assign({}, validGuestUser);
        var password = generator.generate({
          length: 10,
          numbers: true
        });
        guestUser.password = password;
        guestUser.nationality = '';
  
        Users.createGuest(guestUser, function (result) {
          expect(result.statusCode).to.eql(422);
          done();
        });
      });

      it('phone missing request status (422)', function (done) {
        var Users = app.models.Users;
        var guestUser = Object.assign({}, validGuestUser);
        var password = generator.generate({
          length: 10,
          numbers: true
        });
        guestUser.password = password;
        guestUser.phone = '';
  
        Users.createGuest(guestUser, function (result) {
          expect(result.statusCode).to.eql(422);
          done();
        });
      });

      it('email missing request status (422)', function (done) {
        var Users = app.models.Users;
        var guestUser = Object.assign({}, validGuestUser);
        var password = generator.generate({
          length: 10,
          numbers: true
        });
        guestUser.password = password;
        guestUser.email = '';
  
        Users.createGuest(guestUser, function (result) {
          expect(result.statusCode).to.eql(422);
          done();
        });
      });

      it('password missing request status (422)', function (done) {
        var Users = app.models.Users;
        var guestUser = Object.assign({}, validGuestUser);
  
        Users.createGuest(guestUser, function (result) {
          expect(result.statusCode).to.eql(422);
          done();
        });
      });

      it('acceptTerms is false request status (422)', function (done) {
        var Users = app.models.Users;
        var guestUser = Object.assign({}, validGuestUser);
        var password = generator.generate({
          length: 10,
          numbers: true
        });
        guestUser.password = password;
        guestUser.acceptTerms = false;
  
        Users.createGuest(guestUser, function (result) {
          expect(result.statusCode).to.eql(422);
          done();
        });
      });
    });

    describe('Validate guest valid user', function () {
      it('create user as guest is success request status (200)', function (done) {
        var Users = app.models.Users;
        var guestUser = Object.assign({}, validGuestUser);
        var password = generator.generate({
          length: 10,
          numbers: true
        });
        guestUser.password = password;
  
        Users.createGuest(guestUser, function (result) {
          expect(result.statusCode).to.eql(200);
          done();
        });
      });
    });
  });
});

