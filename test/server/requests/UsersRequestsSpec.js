'use strict';

require('../database-cleaner');
var chai = require('chai');
var expect = chai.expect;
var chaiHttp = require('chai-http');
var app = require('../../../server/index');
var baseEndpoint = '/Users';
var moment = require('moment');
var config = require('../../../server/config');
var locales = require('../../../server/locales');

chai.use(chaiHttp);
var localeFormat = 'YYYY-MM-DD';
var yymmdd = moment().format(localeFormat);
var validUser = {
  email: 'elliotalderson@protonmail.com',
  password: 'DolorezHaze',
  birthday: yymmdd,
  acceptTerms: true
}

describe('Users', function () {
  describe('POST /Users', function () {
    describe('given a valid customer', function () {
  

      it('returns an ok status (200)', function (done) {
        chai
        .request(app)
        .post(baseEndpoint)
        .send(validUser)
        .end(function (error, response) {
          expect(response.statusCode).to.eql(200);
          done();
        });
      });
    });

    describe('given an invalid customer', function () {
      it('returns an invalid request status (422)', function (done) {
        var invalidUser = {
          email: '',
          password: 'antikera',
          acceptTerms: true
        };

        chai
        .request(app)
        .post(baseEndpoint)
        .send(invalidUser)
        .end(function (error, response) {
          expect(response.statusCode).to.eql(422);
          expect(response.body.error.name).to.eql('ValidationError');
          done();
        });
      });
    });
  });

  describe('POST /Users/login', function () {
    describe('given a valid customer login details', function () {
      it('email is required', function (done) {
        var Users = app.models.Users;
        var myUser = Object.assign({}, validUser);
        myUser.acceptTerms = true;
        var loginUser = {
          password:validUser.password
        }
        
        Users.create(myUser, function (createError, userCreated) {
          chai
          .request(app)
          .post(baseEndpoint + '/login')
          .send(loginUser)
          .end(function (error, response) {
            var responseText = JSON.parse(response.error.text);
            expect(responseText.error.statusCode).to.eql(400);
            expect(responseText.error.name).to.eql('Error');
            expect(responseText.error.code).to.eql('USERNAME_EMAIL_REQUIRED');
            done();
          });
        });
      });

      it('should login with valid email', function (done) {
        var Users = app.models.Users;
        var myUser = Object.assign({}, validUser);
        myUser.acceptTerms = true;
        var loginUser = {
          email:'elliotalderson[at]protonmail.com',
          password:validUser.password
        }
        
        Users.create(myUser, function (createError, userCreated) {
          chai
          .request(app)
          .post(baseEndpoint + '/login')
          .send(loginUser)
          .end(function (error, response) {
            var responseText = JSON.parse(response.error.text);
            expect(responseText.error.statusCode).to.eql(401);
            expect(responseText.error.name).to.eql('Error');
            expect(responseText.error.code).to.eql('LOGIN_FAILED');
            done();
          });
        });
      });

      it('should login the user success', function(done) {
        var Users = app.models.Users;
        var validCredentials = {
          email: validUser.email,
          password: validUser.password
        };


        Users.create(validUser, function (error, customer) {
          chai
          .request(app)
          .post(baseEndpoint + '/login')
          .send(validCredentials)
          .end(function (error, response) {
            expect(response.statusCode).to.eql(200);
            done();
          });
        });
      });
    });
  });

  describe('POST /Users/reset', function () {
    describe('given a tokenized by reset password', function () {
      it('should request reset password success', function(done) {
        var Users = app.models.Users;
        var emailRequire = {
          email:validUser.email
        }


        Users.create(validUser, function (createError, customer) {
          chai
          .request(app)
          .post(baseEndpoint + '/reset')
          .send(emailRequire)
          .end(function (resetError, response) {
            expect(response.statusCode).to.eql(204);
            done();
          });
        });
      });

      describe('Reset password using forget password function', function () {
        it('should get the details and send to user email', function (done) {
          this.timeout(5000);
          var Users = app.models.Users;
          var myUser = Object.assign({}, validUser);
          myUser.acceptTerms = true;
          var resetPassword = {
            email:validUser.email
          };
    
          Users.create(myUser, function (createError, userCreated) {
            Users.forgetPassword(resetPassword, function (result) {
              expect(result).to.be.a('undefined');
            });
            Users.once('resetPasswordRequest', function (info) {
              var url = `http://${config.host}:${config.port}${baseEndpoint}/${info.accessToken.userId}`;
              var html = `${locales.translatation('Click link to reset your password')}<br/>Click <a href="${url}?access_token=${info.accessToken.id}">here</a> to reset your password`;
              var emailOption = {
                to: info.email,
                from: info.email,
                subject: locales.translatation('Reset password'),
                text: locales.translatation('Click link to reset your password'),
                html: html
              }

              Users.sendEmail(emailOption, function (result) {
                expect(result.statusCode).to.eql(202);
                done();
              });
            });
          });
        });
      });
    });
  });
});