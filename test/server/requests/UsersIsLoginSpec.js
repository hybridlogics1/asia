'use strict';

require('../database-cleaner');
var chai = require('chai');
var expect = chai.expect;
var chaiHttp = require('chai-http');
var app = require('../../../server/index');
var baseEndpoint = '/Users';
var config = require('../../../server/config');
var locales = require('../../../server/locales');
var generator = require('generate-password');

chai.use(chaiHttp);
var validUser = {
  email: 'elliotalderson@protonmail.com',
  password: 'DolorezHaze',
  acceptTerms: true
}

describe('Ask, user now loging in?', function () {
  describe('login by member', function () {
  

    describe(`let's ask, is user logging in?`, function () {
      var Users = app.models.Users;
      var loginUser = {
        email:validUser.email,
        password:validUser.password,
        id:1
      }

      it('Not login yet! request status (401)', function (done) {
        chai
        .request(app)
        .get(baseEndpoint + '/' + loginUser.id)
        .end(function (error, response) {
          expect(response.statusCode).to.eql(401);
          done();
        });
      });

      it('is user member? request status (200)', function (done) {
        Users.create(validUser, function (error) {
          chai
          .request(app)
          .post(baseEndpoint + '/login')
          .send(loginUser)
          .end(function (loginError, loginResponse) {
            chai
            .request(app)
            .get(baseEndpoint + '/' + loginResponse.body.userId + '?access_token=' + loginResponse.body.id)
            .end(function (getError, getGesponse) {
              expect(getGesponse.statusCode).to.eql(200);
              done();
            });
          });
        });
      });
    });
  });

  describe('not a member but need continue as guest', function () {
    describe('let ask, do you a member?', function () {
      var Users = app.models.Users;
      var loginUser = {
        email:validUser.email,
        password:validUser.password,
        id:1
      }
      var validGuestUser = {
        email: 'elliotalderson@protonmail.com',
        firstName: 'FirstName',
        password: generator.generate({ length: 10, numbers: true }),
        lastName: 'LastName',
        passportNumber: 'passsport',
        phone: '+6612345678',
        nationality:'Thai',
        acceptTerms: true
      }


      it('Not login yet! request status (401)', function (done) {
        chai
        .request(app)
        .get(baseEndpoint + '/' + loginUser.id)
        .end(function (error, response) {
          expect(response.statusCode).to.eql(401);
          done();
        });
      });

      it('user continue as guest request status (200)', function (done) {
        Users.createGuest(validGuestUser, function (result) {
          expect(result.statusCode).to.eql(200);
          done();
        });
      });
    });
  });
});