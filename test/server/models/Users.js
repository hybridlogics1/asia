'use strict';

require('../database-cleaner');
var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
var app = require('../../../server/index');
var moment = require('moment');

describe('Users', function () {
  describe('.create', function () {


    var localeFormat = 'YYYY-MM-DD';
    var yymmdd = moment().format(localeFormat);
    var userData = {
      email: 'elliotalderson@protonmail.com',
      password: 'DolorezHaze',
      birthday: yymmdd
    }

    it('Validates Terms Acceptance to be present', function (done) {
      var Users = app.models.Users;
      
      Users.create(userData, function (error) {
        assert(error);
        assert.equal(error.name, 'ValidationError');
        assert.equal(error.statusCode, 422);
        assert.equal(error.details.context, Users.modelName);
        assert.deepEqual(error.details.codes.acceptTerms, ['presence']);
        done();
      });
    });
        
    it('Validates Terms Acceptance to be true', function (done) {
      var Users = app.models.Users;
      var myUser = Object.assign(userData, { acceptTerms: false });

      Users.create(myUser, function (error) {
        assert(error);
        assert.equal(error.name, 'ValidationError');
        assert.equal(error.statusCode, 422);
        assert.equal(error.details.context, Users.modelName);
        assert.deepEqual(error.details.codes.acceptTerms, ['custom.acceptance']);
        done();
      });
    });
        
    it('Validates Birthday to be present', function (done) {
      var Users = app.models.Users;
      var myUser = Object.assign(userData, { birthday: '0-0-0T00:00:00.000Z' });

      Users.create(myUser, function (error) {
        assert(error);
        assert.equal(error.name, 'ValidationError');
        assert.equal(error.statusCode, 422);
        assert.equal(error.details.context, Users.modelName);
        assert.deepEqual(error.details.codes.birthday, ['date']);
        done();
      });
    });
  });
});
