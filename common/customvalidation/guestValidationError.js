function guestValidationError (guestData) {
    var err = new Error();
    err.name = 'ValidationError';
    err.statusCode = 422;
    if( guestData.firstName == null || guestData.firstName == undefined || guestData.firstName == ''){
        err.message = 'The `firstName` should not blank';
    }else if( guestData.lastName == null || guestData.lastName == undefined || guestData.lastName == ''){
        err.message += 'The `lastName` should not blank';
    }else if( guestData.nationality == null || guestData.nationality == undefined || guestData.nationality == ''){
        err.message += 'The `nationality` should not blank';
    }else if( guestData.phone == null || guestData.phone == undefined || guestData.phone == ''){
        err.message += 'The `phone` should not blank';
    }else{
        err = {};
        err = guestData;
        err.statusCode = 200;
    }
    return err;
}

module.exports = guestValidationError;