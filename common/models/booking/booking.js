var _ = require('underscore');
var app = require("../../../server/index");
var moment = require('moment');
const loopback = require('loopback');
var generatePassword = require('password-generator');
//var fx = require('money');
var Email = require("../../../server/utility/email");
module.exports = function (Booking) {

  Booking.beforeRemote('find', function (ctx, status, next) {
    if (ctx.args.filter) {
      ctx.args.filter.include = [{
        relation: "bookingCurrencyIdFkeyrel",
      }, {
        relation: "billingCountryIdFkeyrel"
      }];
    }
    else {
      ctx.args.filter = {
        include: [{
          relation: "bookingCurrencyIdFkeyrel",
        }, {
          relation: "billingCountryIdFkeyrel"
        }]
      }
    }
    next();
  });
  Booking.beforeRemote('findById', function (ctx, status, next) {
    if (ctx.args.filter) {
      ctx.args.filter.include = [{
        relation: "bookingCurrencyIdFkeyrel",
      }, {
        relation: "billingCountryIdFkeyrel"
      }];
    }
    else {
      ctx.args.filter = {
        include: [{
          relation: "bookingCurrencyIdFkeyrel",
        }, {
          relation: "billingCountryIdFkeyrel"
        }]
      }
    }
    next();
  });

  // Booking.observe('before save', function (ctx, next) {
  //   var Products = Booking.app.models.Products;
  //   var ProductVariants = Booking.app.models.ProductVariants;
  //   var Prices = Booking.app.models.Prices;
  //   var Users = Booking.app.models.Users;
  //   var filter = {
  //     where: {
  //       status: true
  //     },
  //     include: [
  //       {
  //         relation: "productsCurrencyIdFkeyrel"
  //       },
  //       {
  //         relation: "variants",
  //         scope: {
  //           include: "price"
  //         }
  //       }

  //     ]
  //   }
  //   if (ctx.isNewInstance) { //add case

  //     Products.findById(ctx.instance.product_id, filter, function (err, product) {
  //       if (err) {
  //         return next(err);
  //       }
  //       if (product) {
  //         //console.log("product found for booking...,",product)
  //         validateBooking(product, ctx.instance, next, function (valid) {
  //           if (valid) {
  //             // check user or register user and validate access token
  //             // if new user send registration email with password
  //             Users.findOne({ where: { email: ctx.instance.email } }, function (err, user) {
  //               if (err) {
  //                 return next(err);
  //               }
  //               if (user) {
  //                 ctx.instance.setAttribute('user_id', user.id);
  //                 return next();
  //               }
  //               else {
  //                 registerUser(ctx.instance, next, function (user) {
  //                   if (err) {
  //                     return next(err)
  //                   }
  //                   if (user) {
  //                     ctx.instance.setAttribute('user_id', user.id);
  //                     return next();
  //                   }
  //                   else {
  //                     console.log("Something went wrong");
  //                     var error = new Error('Some technical error');
  //                     return next(error);
  //                   }
  //                 })
  //               }
  //               console.log(333)
  //             });
  //             console.log(111)
  //           }
  //           else {
  //             console.log("Something went wrong");
  //             var error = new Error('Some technical error');
  //             return next(error);
  //           }
  //         })
  //         console.log("two")
  //       }
  //       else {
  //         console.log("Product not Found");
  //         var error = new Error('Product not Found');
  //         return next(error);
  //       }
  //     });
  //     console.log("thrree")
  //   }
  //   else { // update case
  //     return next();
  //   }
  // });

  Booking.observe('after save', function (ctx, next) {
    if (ctx.instance) {
      app.models.Users.findOne({where:{id:ctx.instance.user_id}},function(err,_user){
        if(err){
          throw err;
        }else{
          app.models.Suppliers.findOne({where:{id:ctx.instance.supplier_id}},function(err,_supplier){
            if(err){
              throw err;
            }else{
      // var url = 'http://' + config.host + ':' + config.port + '/verify?access_token=' + user.verificationToken;
      var data = { name: ctx.instance.billing_first_name, user: _user.email,supplier:_supplier.reservation_email,paragraph7:ctx.instance.id,paragraph8:ctx.instance.adult_pax+ctx.instance.child_pax,paragraph9:ctx.instance.adult_pax,paragraph11:ctx.instance.child_pax,paragraph13:ctx.instance.pickup_time,paragraph14:ctx.instance.pickup_place, paragraph16:ctx.instance.pickup_place };
      Email.send(_user.language_id, data, "booking", function (err, msg) {
        if (err) {
          Booking.destroyAll({id: ctx.instance.id});
            next(err);
        } else {
          console.log("context ",ctx.instance);
            ctx.instance = {
                msg: "Email sent to supplier and user"
            };
            next();
        }
    })
            }
          })          
        }
      })

    }
    else {
      next()
    }
  })

  function validateBooking(product, instance, next, callback) {
    instance.child_price = instance.child_price ? instance.child_price : 0
    instance.infant_price = instance.infant_price ? instance.infant_price : 0
    var variant_id = instance.product_variant_id;
    var price_id = instance.price_id;
    var product_variants = product.variants() ? product.variants() : [];
    //console.log("variants",product_variants,variant_id);
    var variant = _.find(product_variants, function (variant) { return variant.id == variant_id; });
    var product_prices = variant && variant.price() ? variant.price() : [];
    var price = _.find(product_prices, function (price) { return price.id == price_id; });
    var product_currency_id = product.currency_id;
    var booking_method_id = product.booking_method_id;
    if (!variant || !variant.id) {
      var error = new Error('Variant not Found');
      return next(error);
    }
    if (!price || !price.id) {
      var error = new Error('Price not Found');
      return next(error);
    }

    if (instance.adult_pax) {
      if (price.adult_price != instance.adult_price) {
        var error = new Error('Adult Price not Matched');
        return next(error);
      }

      if (price.adult_cost != instance.adult_cost) {
        var error = new Error('Adult Cost not Matched');
        return next(error);
      }
    }

    if (instance.child_pax) {
      if (price.child_price != instance.child_price) {
        var error = new Error('Child Price not Matched');
        return next(error);
      }

      if (price.child_cost != instance.child_cost) {
        var error = new Error('Child Cost not Matched');
        return next(error);
      }
    }

    if (instance.infant_pax) {
      if (price.infant_price != instance.infant_price) {
        var error = new Error('Infant Price not Matched');
        return next(error);
      }

      if (price.infant_cost != instance.infant_cost) {
        var error = new Error('Infant Cost not Matched');
        return next(error);
      }
    }



    if (!booking_method_id || booking_method_id != instance.booking_method_id) {
      var error = new Error('Wrong booking Method option');
      return next(error);
    }
    if (!instance.trip_starts) {
      var error = new Error('Tour Date is required');
      return next(error);
    }
    if (!instance.total) {
      var error = new Error('Positive amount is required for booking');
      return next(error);
    }
    if (!instance.adult_pax && !instance.child_pax) {
      var error = new Error('Either Child or Adult pax are required for booking');
      return next(error);
    }
    if ((product.is_passport_required && !instance.passport_number) || (product.is_pickup_detail_required && !instance.pickup_place) || (product.is_pickup_time_required && !instance.pickup_time) || (product.is_flight_information_required && !user_info.flight_number)) {
      var error = new Error('Incorrect or missing user Details');
      return next(error);
    }
    if (!variant.status) {
      var error = new Error("Product Variant is not Active, This variation can't be booked");
      return next(error);
    }
    var date_excluded = JSON.parse(variant.date_excluded);
    if (date_excluded.indexOf(instance.trip_starts) > 0) {
      var error = new Error("Product Variant is not Available, This variation can't be booked");
      return next(error);
    }

    if (!isVariantAvailable(variant, instance.trip_starts)) {
      var error = new Error("Product Variant is not available on this date, This variation can't be booked");
      return next(error);
    }

    /* Check total calculation */
    var adult_total = instance.adult_pax * fx_convert(price.adult_price, product.productsCurrencyIdFkeyrel().currency_code, instance.booking_currency_code)
    var child_total = 0;
    if (instance.child_pax) {
      child_total = instance.child_pax * fx_convert(price.child_price, product.productsCurrencyIdFkeyrel().currency_code, instance.booking_currency_code)
    }
    var infant_total = 0;
    if (instance.infant_pax) {
      infant_total = instance.infant_pax * fx_convert(price.infant_price, product.productsCurrencyIdFkeyrel().currency_code, instance.booking_currency_code)
    }
    var verify_total = adult_total + child_total + infant_total;

    console.log("verify_total", verify_total, instance.total)
    if (verify_total != instance.total) {
      var error = new Error('Price calculation mismatched');
      return next(error);
    }
    else {
      console.log('rrrrrr')
      return callback(true);
    }
    console.log('11111111')
  }

  function isVariantAvailable(variant, trip_starts) {
    var contract_start = variant.starts_on;
    trip_starts = moment(trip_starts)
    //console.log("before ts date",tour_start_date.toString());
    var tour_can_start = moment(contract_start).add(2, 'days');
    console.log("all the date when checking variants availablity", tour_can_start.toString(), trip_starts.toString());
    if (trip_starts.isAfter(tour_can_start)) {
      return true;
    }
    else {
      return false;
    }
  }

  function registerUser(details, next, callback2) {
    var Users = Booking.app.models.Users;
    var password = generatePassword();
    var data = {
      first_name: details.billing_first_name,
      last_name: details.billing_last_name,
      email: details.email,
      phone: details.billing_phone ? details.billing_phone : null,
      password: password,
      supplier_id: 0,
      country_id: details.nationality,
      passport_number: details.passport_number ? details.passport_number : null
    }
    var where = {
      "where": {
        "email": details.email,
      }
    };
    Users.findOrCreate(where, data, function (err, user) {
      if (err) {
        console.log(err);
        return next(err)
      }
      if (user) {
        return callback2(user);
        // systemPasswordemailNotifier(user,data);  // turn on later
      }
      else {
        var error = new Error('User registration failed');
        return next(error)
      }
    });
  }

  function fx_convert(amount, from, to) {
    return fx(amount).from(from).to(to).toFixed(2);
  }
  //temp solution
  function systemPasswordemailNotifier(user, data) {

    var url = `https://${config.host}:${config.port}${baseEndpoint}/`;
    var html = `Your new password`;
    var emailOption = {
      to: user.email,
      from: "vishal.sharma@theasia.com", //test now
      subject: "Registration Successfull",
      text: data.password,
      html: html
    }
    loopback.Email.send(emailOption, function (error, result) {
      if (error) {

      }
    })
  }

};
