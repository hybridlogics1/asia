var supplier_config = require('../../../server/config/static-credentials');
var _ = require('underscore');
module.exports = function(Suppliers) {
 
    Suppliers.observe('after save', function (ctx, next) {
        if(ctx.instance && ctx.instance.id &&  ctx.instance.name && ctx.instance.reservation_email){
            var Users = Suppliers.app.models.Users;
            var email = ctx.instance.reservation_email;
            var name = ctx.instance.name;
            var supplier_id = ctx.instance.id;
            var password = supplier_config.suppliers.initial_access_value;
            var data = {
                "email":email,
                "first_name" :name,
                "password" : password,
                "supplier_id" : supplier_id
            }
            var where = {
                "where":{
                    "email":email,
                }
            }; 
            Users.findOrCreate(where,data,function(err,response){
                if(err){
                    console.log(err);
                }
            });
        }
        next();
    });
};