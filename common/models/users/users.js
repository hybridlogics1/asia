'use strict';
const loopback = require('loopback');
var app = require("../../../server/index");
var GuestValidationError = require('../../customvalidation/guestValidationError');

var Email = require("../../../server/utility/email");
var config = require('../../../server/config.json');
var roles = require("../../../server/utility/roles");
var https = require('https');
var _ = require('lodash');
module.exports = function (Users) {
    // Users.validatesPresenceOf('acceptTerms', { message: 'Cannot be blank' });
    // Users.validate('acceptTerms', validateTermsAcceptance);

    // function validateTermsAcceptance (error) {
    //   var fieldValue = this.acceptTerms;
    //   if (fieldValue !== undefined && fieldValue === false ) {
    //     error('acceptance');
    //   }
    // }
    Users.observe('after save', function (ctx, next) {
        if (ctx.isNewInstance && ctx.instance.user_type != 1) {      
        Users.resetPassword({email: ctx.instance.email},function(err,resp){
            if(err){
                next(err);
            }else{
                next();
            }
        })
        } else {
            next();
        }

      });
      Users.beforeRemote('create', function (ctx, user, next) {
          ctx.args.data.user_type=1;
          next();
      });
    Users.afterRemote('create', function (ctx, user, next) {
        Users.generateVerificationToken(user, function (err, token) {
            if (err) {
                return fn(err);
            }

            user.verificationToken = token;
            user.save(function (err) {
                if (err) {
                } else {
                    var url = 'http://' + config.host + ':' + config.port + '/verify?access_token=' + user.verificationToken;
                    var data = { name: user.first_name, url: url, to: user.email };
                    // 1 means sending Email in english
                    Email.send(user.language_id, data, "verification", function (err, msg) {
                        if (err) {
                            Users.destroyAll({ id: user.id });
                            next(err);
                        } else {
                            ctx.result = {
                                msg: "An Email has been sent to you Please verify your Email"
                            };
                            next();
                        }
                    })
                }
            });
        });
    });

    Users.beforeRemote('find', function (ctx, user, next) {
        if (ctx.args.filter && ctx.args.filter.where) {
            ctx.args.filter.where['supplier_id'] = 0
        }
        else {
            ctx.args.filter = {
                where: { supplier_id: 0 }
            }
        }
        next();
    });


    Users.isLogin = function (userData, callback) {
        Users.findOne(userData, function (error, user) {
            callback(user);
        });
    };

    Users.createGuest = function (guestData, callback) {
        var customError = GuestValidationError(guestData);
        if (customError.statusCode !== 200) {
            callback(customError);
        } else {
            Users.create(guestData, function (error, userResult) {
                if (error) {
                    callback(error);
                } else {
                    var validUserCreated = GuestValidationError(userResult);
                    callback(validUserCreated);
                }
            });
        }
    }
    //send password reset link when requested
Users.on('resetPasswordRequest', function (info, cb) {
        var url = 'http://' + config.host + ':' + config.port + '/reset-password?access_token=' + info.accessToken.id;
        var data = { url: url, to: info.email };
        //1 Means sending emaail in english
        Users.findOne({where:{email:info.email}},function(err,userObj){
        if(err){
            cb("Email is not sent", null);
        }else{
            Email.send(userObj.language_id, data, "reset_password", function (err, msg) {
            if (err) {
                return;
            } else {
                cb(null, "Email sent to ypur EmailAddress");
            }
        })
        }
    });
        
    });


    Users.forgetPassword = function (resetData, callback) {
        Users.resetPassword(resetData, function (error) {
            callback(error);
        });
    };


        Users.sendEmail = function (emailOption, callback) {
        loopback.Email.send(emailOption, function (error, result) {
            if (!error) {
                // the request has been accepted for processing
                callback({statusCode: 202, message: 'email sent'});
            }
        });
    };


    Users.remoteMethod('sendEmail', {
        accepts: [{ arg: 'data', type: 'object', http: { source: 'body' } }],
        returns: { arg: 'response', type: 'string', root: true },
        http: {
            path: '/send-email',
            verb: 'post'
        },
    });


    Users.userRole = function (userId, roleName, callback) {
        if (roleName == "admin") {
            roles.adminRole(userId, function (err, docs) {
                if (err) {
                    throw err;
                } else {
                    callback(null, "Success");
                }
            })
        }
        else if (roleName == "viewer") {
            roles.viewerRole(userId, function (err, docs) {
                if (err) {
                    throw err;
                } else {
                    callback(null, "Success");
                }
            })
        }
        else if (roleName == "creator") {
            roles.creatorRole(userId, function (err, docs) {
                if (err) {
                    throw err;
                } else {
                    callback(null, "Success");
                }
            })
        }
        else if (roleName == "editor") {
            roles.editorRole(userId, function (err, docs) {
                if (err) {
                    throw err;
                } else {
                    callback(null, "Success");
                }
            })
        } else {
            callback(null, "Please enter correct role");
        }
    };

    Users.remoteMethod('userRole', {
        accepts: [{ arg: 'userId', type: 'string' },
        { arg: 'roleName', type: 'string' }],
        returns: [{ arg: 'userId', type: 'string' },
        { arg: 'roleName', type: 'string' }]
    });

    Users.changePassword = function (ctx, oldPassword, newPassword, confirmPassword, callback) {
        //Changing/updating password loopback
        // ctx contains token of whom password is going to be changed (To set userId)
        if (ctx.req.accessToken != undefined && !_.isEmpty(ctx.req.accessToken)) {
            Users.findOne({ where: { id: ctx.req.accessToken.userId } }, function (err, _user) {
                if (err) throw err;
                else {
                    if (newPassword !== confirmPassword) {
                        callback("password does not match", null);
                    } else {
                        try {
                            _user.hasPassword(
                                oldPassword,
                                function (err, isMatch) {
                                    if (!isMatch) {
                                        callback("password is invalid", null);
                                    } else {
                                        _user.updateAttribute('password', newPassword, function (err, nuser) {
                                            if (err) throw error;
                                            else {
                                                callback(null, "password reset successfully");
                                            }
                                        });
                                    }
                                });
                        } catch (error) {
                            callback("There is an error in resetting password", null);
                        }
                    }
                }
            });
        } else {
            var err = new Error('Unauthorized Access');
            err.statusCode = 401;
            err.code = 'Unauthorized';
            callback(err, null);
        };

    };


    Users.remoteMethod('changePassword', {
        description: 'Changes the Password of user',
        accepts: [{ arg: 'ctx', type: 'object', http: { source: 'context' } },
        { arg: 'oldPassword', type: 'string' },
        { arg: 'newPassword', type: 'string' },
        { arg: 'confirmPassword', type: 'string' },],
        returns: [{ arg: 'oldPassword', type: 'string' },
        { arg: 'newPassword', type: 'string' },
        { arg: 'confirmPassword', type: 'string' }],
        http: { verb: 'post' },
    });

    Users.socialLogin = function (fbObj, fn) {
        //fbObj
        //{ "id":"facbook provided Id",
        //  "email":"facebook provided Email",
        //  "type":"facebook/gmail",
        //  "fbauth":"Token provided by Facebook"}
        var self = this;
        fn = fn || utils.createPromiseCallback();
        var include = [];

        function tokenHandler(err, token) {
            if (err) return fn(err);
            if (Array.isArray(include) ?
                include.indexOf('user') !== -1 : include === 'user') {
                // NOTE(bajtos) We can't set token.user here:
                //  1. token.user already exists, it's a function injected by
                //     "AccessToken belongsTo User" relation
                //  2. ModelBaseClass.toJSON() ignores own properties, thus
                //     the value won't be included in the HTTP response
                // See also loopback#161 and loopback#162
                token.__data.user = user;
            }
            fn(err, token);
        }

        self.findOne({ where: { email: fbObj.email, social_id: fbObj.id, social_type: fbObj.type } }, function (err, user) {
            if (err) {
                fn(err);
            } else if (user) {
                debug('User found');
                user.createAccessToken(DEFAULT_TTL, tokenHandler);
                //fn(null,true);
            } else {
                https.get('https://graph.facebook.com/me?access_token=' + fbObj.fbauth, function (response) {
                    response.setEncoding('utf8');
                    response.on('data', function (data) {
                        data = JSON.parse(data);
                        if (data.error != undefined && data.error.code == 190) {
                            var err = new Error('Invalid token: %s', fbObj.fbauth);
                            fn(err, null);
                        } else {
                            if (fbObj.id == data.id) {
                                User.create({
                                    email: fbObj.email,
                                    password: fbObj.id + Date.now(),
                                    emailVerified: true,
                                    social_id: fbObj.id,
                                    social_type: fbObj.type
                                }, function (err, userInstance) {
                                    if (err) {
                                        fn(err);
                                    } else {
                                        userInstance.createAccessToken(DEFAULT_TTL, tokenHandler);
                                        //fn(null,true);
                                    }
                                });
                            } else {
                                var err = new Error('Invalid Id: ', fbObj.id);
                                fn(err, null);
                            };
                        };
                    });
                    response.on('error', console.error);
                });
            }
        });
        return fn.promise;
    };

    Users.remoteMethod('socialLogin', {
        description: 'Login/Signup with social platform',
        accepts: [
            { arg: 'fbObj', type: 'object', http: { source: 'body' }, required: true }
        ],
        returns: {
            arg: 'accessToken', type: 'object', root: true,
            description: 'The response body contains properties of the ' +
                '{{AccessToken}} created on login.\n' +
                'Depending on the value of `include` parameter, ' +
                'the body may contain additional properties:\n\n' +
                '  - `user` - `U+007BUserU+007D` - Data of ' +
                'the currently logged in user. ' +
                '{{(`include=user`)}}\n\n',
        },
        http: { verb: 'post', path: '/socialLogin' },
    });
}
