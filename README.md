## Start api server
######*prod / dev*
```
$ npm start

## Update database on model changes (local)
```
$ npm start autoupdate
```

API EXPLORER :  http://localhost:3003/explorer
